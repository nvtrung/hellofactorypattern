package dao;

public class DatasourceFactory {
    public IDatasource getDatasource(String typeOfDatasource) throws Exception {
        IDatasource ds;
        if ("FakeDatasource".equals(typeOfDatasource)) {
            ds = new FakeDatasource();
        } else if ("TextDatasource".equals(typeOfDatasource)) {
            ds = new TextDatasource();
        } else {
            throw new Exception(String.format("Chưa cài đặt loại datasource [%s]", typeOfDatasource));
        }
        return ds;
    }
}
