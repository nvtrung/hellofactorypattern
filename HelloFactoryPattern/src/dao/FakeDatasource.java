package dao;

import entity.SinhVien;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FakeDatasource implements  IDatasource {
    /**
     * Đọc từ datasource tất cả sinh viên
     *
     * @return
     */
    @Override
    public List<SinhVien> getAll() {
        List<SinhVien> lst = new ArrayList<>();
        SinhVien x1 = new SinhVien("Sinh vien thứ nhất", true, new Date());
        SinhVien x2 = new SinhVien("Sinh vien thứ hai", false, new Date());
        SinhVien x3 = new SinhVien("Sinh vien thứ ba", true, new Date());
        SinhVien x4 = new SinhVien("Sinh vien thứ bốn", true, new Date());
        SinhVien x5 = new SinhVien("Sinh vien thứ năm", false, new Date());
        lst.add(x1);
        lst.add(x2);
        lst.add(x3);
        lst.add(x4);
        lst.add(x5);

        return lst;
    }

    /**
     * Lưu danh sách sinh viên trở lại datasource
     *
     * @param lst
     */
    @Override
    public void save(List<SinhVien> lst) {
        System.out.println(String.format("Đã lưu %d phần tử vào CSDL", lst.size()));
    }
}
