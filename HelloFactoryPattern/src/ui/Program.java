package ui;

import dao.DatasourceFactory;
import dao.IDatasource;
import entity.SinhVien;

import java.util.List;

public class Program {
    public static void main(String[] args){
        // region IDatasource = tạo ra đối tượng datasource
        DatasourceFactory df = new DatasourceFactory();
        IDatasource ds = null;
        try {
            String typeOfDatasource = ""; // Đọc từ file cấu hình
            typeOfDatasource = "TextDatasource";

//            ds = df.getDatasource("FakeDatasource");
            ds = df.getDatasource(typeOfDatasource);
        } catch (Exception e) {
            System.out.println("Lỗi khi khởi tạo datasource");
            e.printStackTrace();
            return;
        }
        //endregion

        // region Sử dụng datasource
        List<SinhVien> lst = ds.getAll();

        // In danh sách các sinh viên đọc được
        for(SinhVien x: lst){
            System.out.println(String.format("Họ tên: %s, Giới tính: %s, Ngày sinh: %s",
                    x.getHoTen(), x.isGioiTinhNam(), x.getNgaySinh()));
        }

        // Xử lý danh sách sinh viên: thêm, xoá, sửa
        SinhVien sv =new SinhVien("Nguyễn Văn", true, new java.util.Date());
        lst.add(sv);

        // Lưu danh sách sinh viên trở lại datasource
        ds.save(lst);
        //endregion
    }
}
