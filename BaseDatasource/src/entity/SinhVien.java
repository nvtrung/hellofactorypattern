package entity;

import java.util.Date;

public class SinhVien {
    private String hoTen;
    private boolean gioiTinhNam;
    private Date ngaySinh;

    public SinhVien(String hoTen, boolean gioiTinhNam, Date ngaySinh){
        this.setHoTen(hoTen);
        this.setGioiTinhNam(gioiTinhNam);
        this.setNgaySinh(ngaySinh);
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public boolean isGioiTinhNam() {
        return gioiTinhNam;
    }

    public void setGioiTinhNam(boolean gioiTinhNam) {
        this.gioiTinhNam = gioiTinhNam;
    }

    public Date getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(Date ngaySinh) {
        this.ngaySinh = ngaySinh;
    }
}
