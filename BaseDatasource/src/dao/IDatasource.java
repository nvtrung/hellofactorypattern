package dao;

import entity.SinhVien;

import java.util.List;

public interface IDatasource {
    /**
     * Đọc từ datasource tất cả sinh viên
     * @return
     */
    public List<SinhVien> getAll();

    /**
     * Lưu danh sách sinh viên trở lại datasource
     * @param lst
     */
    public void save(List<SinhVien> lst);
}
